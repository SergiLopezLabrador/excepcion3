package dto;

import exceptions.excepcionNueva;

public class ejecucionProgramaDto {
	
	//Aqu� voy a crear un m�todo donde agrupar� todos los m�todos y los encapsular� en un solo m�todo
	//para posteriormente ejecutarlo al Main
	public static void ejecutarPrograma() {
		
		//Aqu� creo la variable tipo String que ser� analizada
        int numAleatorio = numAleatorioDto.numeroRandom();
        
        
        try {
        	//Aqu� uso un try para analizar la variable
        	numAleatorio = numAleatorioDto.numeroRandom();
 
        	//En el caso de que la palabra sea "Sergi", se enviar� una se�al a la expeci�n de error
            if (numAleatorio%2==0) {
                throw new excepcionNueva("error");     
            } 
            else if (numAleatorio%3==0) {
                throw new excepcionNueva("error1");     
            } 
        } 
        
        //Finalmente, mostramos el mensaje del error por consola
        catch (excepcionNueva numError) {
        	System.out.println("Generando n�mero aleatorio...");
        	System.out.println("El n�mero aleatorio generado �s: " + numAleatorio);
            System.out.println(numError.MensajeInformativo());
        }
        
        //Aqu� muestro que el programa a finalizado
        System.out.println("Programa terminado");
		

    }

}
