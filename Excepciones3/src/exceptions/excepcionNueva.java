package exceptions;

public class excepcionNueva extends Exception{
    
	//Aqu� creo la variable tipo String tipo private
    private String MensajeInformativo;
     
    //Aqu� creo un constructor donde le otorgo valor a la variable 
    public excepcionNueva(String MensajeInformativo){
    	
        this.MensajeInformativo=MensajeInformativo;
    }
     
    //Aqu� creo un m�todo donde genero un mensaje de error al recibir el parametro en formatro string que 
    //proviene del formato padre
    
    public String MensajeInformativo(){
         
    	//Aqu� creo la variable que utilizar� para crear el mensaje de error
        String Informacion="Prueba";
         
        //Aqu� creo un switch case donde su objetivo �s mostrar el mensaje cuando reciba el parametro
        //"error1" o "error2" (en mi caso)
        switch(MensajeInformativo){
            case "error":
            	Informacion="Es par";
                break;   
            case "error1":
            	Informacion="Es impar";
                break;
        }
         
        //Finalmente devolvemos el mensaje
        return Informacion;
         
    }
     
}